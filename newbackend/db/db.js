const mongoose = require('mongoose');

mongoose.connect(
  'mongodb+srv://logintest:test123@cluster0.ou8xn.mongodb.net/logintest?retryWrites=true&w=majority',
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
  },
);

module.exports = {mongoose};
