const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const {mongoose} = require('./db/db');
const userController = require('./controllers/userController');

const port = process.env.PORT || 5000;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/user', userController);

app.listen(port, () => {
  console.log('Server is running on PORT 5000');
});
