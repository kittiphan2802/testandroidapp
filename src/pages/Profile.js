import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {logoutUser} from '../actions/auth.actions';
import RNRestart from 'react-native-restart';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyle: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: '500',
  },
  button: {
    width: 300,
    backgroundColor: '#1c313a',
    borderRadius: 15,
    marginVertical: 16,
    paddingVertical: 14,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
});
class Profile extends Component {
  logoutUser = () => {
    this.props.dispatch(logoutUser());
    RNRestart.Restart();
  };
  render() {
    const {
      getUser: {userDetails},
    } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.textStyle}>This is a Profile Page </Text>
        <TouchableOpacity style={styles.button} onPress={this.logoutUser}>
          <Text style={styles.buttonText}>Log out</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

mapStateToProps = state => ({
  getUser: state.userReducer.getUser,
});

mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
