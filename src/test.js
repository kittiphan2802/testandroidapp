import React, {Component} from 'react';
import {Button, Text, View} from 'react-native';

export default class Test1 extends Component {
  render() {
    return (
      <View>
        <Text> Test1 </Text>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Signup</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class Test2 extends Component {
  render() {
    return (
      <View>
        <Text> Test2 </Text>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Signup</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();
export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Test1}
            options={{title: 'Welcome'}}
          />
          <Stack.Screen name="Profile" component={Test2} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
