import React, {Component} from 'react';
import {connect} from 'react-redux';
import Routers from './component/Routes';
import Home from './pages/Profile';

class Main extends Component {
  render() {
    const {
      authData: {isLoggedIn},
    } = this.props;
    return <Routers isLoggedIn={isLoggedIn ? 'Profile' : 'AuthRoute'} />;
  }
}

mapStateToProps = state => ({
  authData: state.authReducer.authData,
});

export default connect(mapStateToProps, null)(Main);
