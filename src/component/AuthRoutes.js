import React, {Component} from 'react';
import Login from '../pages/Login';
import Signup from '../pages/Signup';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

export default class Routes extends Component {
  render() {
    return (
      <Stack.Navigator headerMode={null}>
        <Stack.Screen name="Signup" component={Signup} />
        <Stack.Screen name="Login" component={Login} />
      </Stack.Navigator>
    );
  }
}
