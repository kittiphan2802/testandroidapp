import React, {Component} from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';

export default class logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{width: 200, height: 200, borderRadius: 75}}
          source={require('../images/Logo.png')}
        />
        <Text style={styles.logoText}>Welcome to KTP Shop</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoText: {
    marginVertical: 15,
    fontSize: 24,
    color: 'rgba(255,255,255,0.7)',
  },
});
