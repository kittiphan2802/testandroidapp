import React, {Component} from 'react';
import AuthReducer from '../component/AuthRoutes';
import Profile from '../pages/Profile';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

export default class Routes extends Component {
  render() {
    const isLoggedIn = this.props.isLoggedIn;
    console.log('loginUser :', isLoggedIn);
    return (
      <NavigationContainer>
        <Stack.Navigator headerMode={null} initialRouteName={isLoggedIn}>
          <Stack.Screen name="AuthReducer" key="root" component={AuthReducer} />
          <Stack.Screen name="Profile" key="app" component={Profile} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
